#include <SparseSmooth/SparseSmooth.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace SparseSmooth;

int main(int argc,char** argv) {
  if(argc!=2) {
    std::cout << "Usage: mainSparseSmooth XXX.obj" << std::endl;
  } else {
    std::ifstream is(argv[1]);
    SparseSmoothProblem::Vec3T v;
    SparseSmoothProblem::Vec3i f;
    std::vector<SparseSmoothProblem::Vec3T> vss;
    std::vector<SparseSmoothProblem::Vec3i> fss;

    char c;
    std::string line;
    while(std::getline(is,line))
      if(line.size()>2 && line[0]=='v') {
        std::istringstream iss(line);
        iss >> c >> v[0] >> v[1] >> v[2];
        vss.push_back(v);
      } else if((int)line.size()>2 && line[0]=='f') {
        std::istringstream iss(line);
        iss >> c >> f[0] >> f[1] >> f[2];
        fss.push_back(f-SparseSmoothProblem::Vec3i::Ones());
      }
    SparseSmoothProblem prob(vss,fss);
    PMSparseSmoothSolver sol(prob);
    //ADMMSparseSmoothSolver sol(prob);
    sol.lambda()=1000;
    //sol.beta0()=100000;
    //ExactSparseSmoothSolver sol(prob);
    //PatchLimitedExactSmoothSolver sol(prob);
    SparseSmoothProblem::Vec z=sol.solve();
    prob.writeObj("output.obj",z);
  }
}
