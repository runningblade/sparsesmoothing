#ifndef MIP_sSPARSE_SMOOTH_H
#define MIP_SPARSE_SMOOTH_H

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <vector>

#define DECL_TYPES_T(FLOAT) \
typedef FLOAT T; \
typedef Eigen::Matrix<T,2,2> Mat2T; \
typedef Eigen::Matrix<T,3,3> Mat3T; \
typedef Eigen::Matrix<T,2,3> Mat2X3T; \
typedef Eigen::Matrix<T,3,-1> Mat3XT; \
typedef Eigen::Matrix<T,-1,1> Vec; \
typedef Eigen::Matrix<T,2,1> Vec2T; \
typedef Eigen::Matrix<T,3,1> Vec3T; \
typedef Eigen::Matrix<int,2,1> Vec2i; \
typedef Eigen::Matrix<int,3,1> Vec3i; \
typedef Eigen::Matrix<int,2,-1> Mat2Xi; \
typedef Eigen::Matrix<int,3,-1> Mat3Xi; \
typedef Eigen::SparseMatrix<T,0,int> SMatT;

namespace SparseSmooth {
struct SparseSmoothProblem {
  DECL_TYPES_T(double)
  SparseSmoothProblem(const Mat3XT& vss,const Mat3Xi& iss);
  SparseSmoothProblem(const std::vector<Vec3T>& vss,const std::vector<Vec3i>& iss);
  void writeObj(const std::string& path,const Vec& d) const;
  const Mat3XT& vss() const;
  const Mat3Xi& iss() const;
  const Mat2Xi& ess() const;
  Mat2X3T getD(int i) const;
  Vec2T xy(int i) const;
  SMatT getMass() const;
  T area(int i) const;
 private:
  void buildEdge();
  Mat3XT _vss;
  Mat3Xi _iss;
  Mat2Xi _ess;
};
//local solver
struct PMSparseSmoothSolver {
  DECL_TYPES_T(double)
  PMSparseSmoothSolver(const SparseSmoothProblem& prob);
  void assembleD(int i,int r,std::vector<Eigen::Triplet<T,int>>& trips) const;
  const T& lambda() const;
  T& lambda();
  const T& beta0() const;
  T& beta0();
  const T& betaMax() const;
  T& betaMax();
  const T& betaInc() const;
  T& betaInc();
  const T& eps() const;
  T& eps();
  virtual Vec solve() const;
 protected:
  const SparseSmoothProblem& _prob;
  T _lambda,_beta0,_betaMax,_betaInc,_eps;
};
struct ADMMSparseSmoothSolver : public PMSparseSmoothSolver {
  DECL_TYPES_T(double)
  ADMMSparseSmoothSolver(const SparseSmoothProblem& prob);
  virtual Vec solve() const;
};
//global solver
struct ExactSparseSmoothSolver {
  DECL_TYPES_T(double)
  ExactSparseSmoothSolver(const SparseSmoothProblem& prob);
  const T& lambda() const;
  T& lambda();
  T getBigM() const;
  virtual Vec solve() const;
 protected:
  const SparseSmoothProblem& _prob;
  T _lambda;
};
struct PatchLimitedExactSmoothSolver : public ExactSparseSmoothSolver {
  DECL_TYPES_T(double)
  PatchLimitedExactSmoothSolver(const SparseSmoothProblem& prob);
  const int& nPatch() const;
  int& nPatch();
  virtual Vec solve() const;
 protected:
  int _nPatch;
};
}

#endif
