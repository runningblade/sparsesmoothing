#include "SparseSmooth.h"
#include <gurobi_c++.h>
#include <unordered_map>
#include <fstream>

namespace SparseSmooth {
struct EdgeHash {
  size_t operator()(const Eigen::Matrix<int,2,1>& key) const {
    std::hash<int> h;
    return h(key[0])+h(key[1]);
  }
  bool operator()(const Eigen::Matrix<int,2,1>& a,const Eigen::Matrix<int,2,1>& b) const {
    for(int i=0; i<2; i++)
      if(a[i]<b[i])
        return true;
      else if(a[i]>b[i])
        return false;
    return false;
  }
};
//SparseSmoothProblem
SparseSmoothProblem::SparseSmoothProblem(const Mat3XT& vss,const Mat3Xi& iss):_vss(vss),_iss(iss) {
  buildEdge();
}
SparseSmoothProblem::SparseSmoothProblem(const std::vector<Vec3T>& vss,const std::vector<Vec3i>& iss) {
  int id=0;
  _vss.resize(3,vss.size());
  for(Vec3T v:vss)
    _vss.col(id++)=v;

  id=0;
  _iss.resize(3,iss.size());
  for(Vec3i i:iss)
    _iss.col(id++)=i;

  buildEdge();
}
void SparseSmoothProblem::writeObj(const std::string& path,const Vec& d) const {
  std::ofstream os(path);
  for(int i=0; i<_vss.cols(); i++)
    os << "v " << _vss(0,i) << " " << _vss(1,i) << " " << d[i] << std::endl;
  for(int i=0; i<_iss.cols(); i++)
    os << "f " << _iss(0,i)+1 << " " << _iss(1,i)+1 << " " << _iss(2,i)+1 << std::endl;
}
const SparseSmoothProblem::Mat3XT& SparseSmoothProblem::vss() const {
  return _vss;
}
const SparseSmoothProblem::Mat3Xi& SparseSmoothProblem::iss() const {
  return _iss;
}
const SparseSmoothProblem::Mat2Xi& SparseSmoothProblem::ess() const {
  return _ess;
}
SparseSmoothProblem::Mat2X3T SparseSmoothProblem::getD(int i) const {
  Mat2T a;
  a.row(0)=xy(_iss(1,i))-xy(_iss(0,i));
  a.row(1)=xy(_iss(2,i))-xy(_iss(0,i));

  Mat2X3T b;
  b.setZero();
  b(0,0)=-1;
  b(0,1)=1;
  b(1,0)=-1;
  b(1,2)=1;

  return a.inverse()*b;
}
SparseSmoothProblem::Vec2T SparseSmoothProblem::xy(int i) const {
  return _vss.block<2,1>(0,i);
}
SparseSmoothProblem::SMatT SparseSmoothProblem::getMass() const {
  SMatT H;
  H.resize(_vss.cols(),_vss.cols());
  std::vector<Eigen::Triplet<T,int>> trips;
  for(int i=0; i<_iss.cols(); i++) {
    Mat3T M;
    M.setConstant(1/6.0);
    M.diagonal().setConstant(1/3.0);
    M*=area(i);
    for(int r=0; r<3; r++)
      for(int c=0; c<3; c++)
        trips.push_back(Eigen::Triplet<T,int>(_iss(r,i),_iss(c,i),M(r,c)));
  }
  H.setFromTriplets(trips.begin(),trips.end());
  return H;
}
SparseSmoothProblem::T SparseSmoothProblem::area(int i) const {
  Mat2T d;
  d.col(0)=xy(_iss(1,i))-xy(_iss(0,i));
  d.col(1)=xy(_iss(2,i))-xy(_iss(0,i));
  return fabs(d.determinant())/2;
}
void SparseSmoothProblem::buildEdge() {
  int nrE=0;
  //build
  std::unordered_map<Vec2i,Vec2i,EdgeHash> emap;
  for(int c=0; c<_iss.cols(); c++)
    for(int d=0; d<3; d++) {
      Vec2i key(_iss((d+1)%3,c),_iss((d+2)%3,c));
      if(key[0]>key[1])
        std::swap(key[0],key[1]);
      if(emap.find(key)==emap.end())
        emap[key]=Vec2i(c,-1);
      else {
        assert(emap[key][1]==-1);
        emap[key][1]=c;
        nrE++;
      }
    }
  //assemble
  _ess.resize(2,nrE);
  nrE=0;
  for(std::pair<Vec2i,Vec2i> e:emap)
    if(e.second[1]>=0) {
      _ess(0,nrE)=e.second[0];
      _ess(1,nrE)=e.second[1];
      nrE++;
    }
}
//PMSparseSmoothSolver
PMSparseSmoothSolver::PMSparseSmoothSolver(const SparseSmoothProblem& prob):_prob(prob),_lambda(10),_beta0(1e1),_betaMax(1e4),_betaInc(2),_eps(1e-5) {}
const PMSparseSmoothSolver::T& PMSparseSmoothSolver::lambda() const {
  return _lambda;
}
PMSparseSmoothSolver::T& PMSparseSmoothSolver::lambda() {
  return _lambda;
}
const PMSparseSmoothSolver::T& PMSparseSmoothSolver::beta0() const {
  return _beta0;
}
PMSparseSmoothSolver::T& PMSparseSmoothSolver::beta0() {
  return _beta0;
}
const PMSparseSmoothSolver::T& PMSparseSmoothSolver::betaMax() const {
  return _betaMax;
}
PMSparseSmoothSolver::T& PMSparseSmoothSolver::betaMax() {
  return _betaMax;
}
const PMSparseSmoothSolver::T& PMSparseSmoothSolver::betaInc() const {
  return _betaInc;
}
PMSparseSmoothSolver::T& PMSparseSmoothSolver::betaInc() {
  return _betaInc;
}
const PMSparseSmoothSolver::T& PMSparseSmoothSolver::eps() const {
  return _eps;
}
PMSparseSmoothSolver::T& PMSparseSmoothSolver::eps() {
  return _eps;
}
void PMSparseSmoothSolver::assembleD(int i,int r,std::vector<Eigen::Triplet<T,int>>& trips) const {
  Mat2X3T D=_prob.getD(i);
  for(int R=0; R<2; R++)
    for(int c=0; c<3; c++)
      trips.push_back(Eigen::Triplet<T,int>(r+R,_prob.iss()(c,i),D(R,c)));
}
PMSparseSmoothSolver::Vec PMSparseSmoothSolver::solve() const {
  //we need to minimize:
  //argmin_{z,d} (z-z0)^T*H*(z-z0)/2 + beta*(D*z-d)^2/2 + lambda*|d1-d2|_0
  //
  //step 1: fix d and optimize z
  //H*(z-z0)+beta*D^T*(D*z-d)=0
  //(H+beta*D^T*D)*z=H*z0+beta*D^T*d
  //
  //step 2: fix z and optimize d
  //each <4,1> block of d is updated separately by choosing from d1=d2 or d1!=d2
  SMatT H=_prob.getMass(),D;
  std::vector<Eigen::Triplet<T,int>> trips;
  D.resize(_prob.ess().cols()*4,_prob.vss().cols());
  for(int c=0; c<_prob.ess().cols(); c++) {
    assembleD(_prob.ess()(0,c),c*4+0,trips);
    assembleD(_prob.ess()(1,c),c*4+2,trips);
  }
  D.setFromTriplets(trips.begin(),trips.end());
  //main loop
  T beta=beta0();
  Eigen::SimplicialCholesky<SMatT> inv(H+D.transpose()*D*beta);
  Vec z=_prob.vss().row(2),z0=z,d=D*z,zLast,dLast;
  while(true) {
    //update z
    z=inv.solve(H*z0+beta*D.transpose()*d);
    //update d
    d=D*z;
    for(int r=0; r<d.size(); r+=4) {
      //case 1:
      T e1=lambda();
      //case 2:
      Vec2T d1=d.segment<2>(r+0),d2=d.segment<2>(r+2),dMean=(d1+d2)/2;
      T e2=beta*((d1-dMean).squaredNorm()+(d2-dMean).squaredNorm())/2;
      //choose
      if(e2<e1) {
        d.segment<2>(r+0)=dMean;
        d.segment<2>(r+2)=dMean;
      }
    }
    //termination
    if(zLast.size()==z.size() && (zLast-z).cwiseAbs().maxCoeff()<eps() && (dLast-d).cwiseAbs().maxCoeff()<eps()) {
      if(beta<betaMax()) {
        beta*=betaInc();
        std::cout << "betaUpdate=" << beta << std::endl;
        inv.compute(H+D.transpose()*D*beta);
      } else break;
    }
    if(zLast.size()==z.size())
      std::cout << "PMIter zUpdate=" << (zLast-z).cwiseAbs().maxCoeff() << " dUpdate=" << (dLast-d).cwiseAbs().maxCoeff() << std::endl;
    //record
    zLast=z;
    dLast=d;
  }
  return z;
}
//ADMMSparseSmoothSolver
ADMMSparseSmoothSolver::ADMMSparseSmoothSolver(const SparseSmoothProblem& prob):PMSparseSmoothSolver(prob) {}
ADMMSparseSmoothSolver::Vec ADMMSparseSmoothSolver::solve() const {
  //we need to minimize:
  //argmin_{z,d} (z-z0)^T*H*(z-z0)/2 + beta*(D*z-d)^2/2 + gamma^T*(D*z-d) + lambda*|d1-d2|_0
  //            =(z-z0)^T*H*(z-z0)/2 + beta*(D*z-d+gamma/beta)^2/2 + lambda*|d1-d2|_0
  //
  //step 1: fix d and optimize z
  //H*(z-z0)+beta*D^T*(D*z-d+gamma/beta)=0
  //(H+beta*D^T*D)*z=H*z0+beta*D^T*(d-gamma/beta)
  //
  //step 2: fix z and optimize d
  //each <4,1> block of d is updated separately by choosing from d1=d2 or d1!=d2
  //
  //step 3: update gamma
  //gamma+=beta*(D*z-d)
  SMatT H=_prob.getMass(),D;
  std::vector<Eigen::Triplet<T,int>> trips;
  D.resize(_prob.ess().cols()*4,_prob.vss().cols());
  for(int c=0; c<_prob.ess().cols(); c++) {
    assembleD(_prob.ess()(0,c),c*4+0,trips);
    assembleD(_prob.ess()(1,c),c*4+2,trips);
  }
  D.setFromTriplets(trips.begin(),trips.end());
  //main loop
  T beta=beta0();
  Eigen::SimplicialCholesky<SMatT> inv(H+D.transpose()*D*beta);
  Vec z=_prob.vss().row(2),z0=z,d=D*z,zLast,dLast,gamma=Vec::Zero(d.size());
  while(true) {
    //update z
    z=inv.solve(H*z0+beta*D.transpose()*(d-gamma/beta));
    //update d
    d=D*z+gamma/beta;
    for(int r=0; r<d.size(); r+=4) {
      //case 1:
      T e1=lambda();
      //case 2:
      Vec2T d1=d.segment<2>(r+0),d2=d.segment<2>(r+2),dMean=(d1+d2)/2;
      T e2=beta*((d1-dMean).squaredNorm()+(d2-dMean).squaredNorm())/2;
      //choose
      if(e2<e1) {
        d.segment<2>(r+0)=dMean;
        d.segment<2>(r+2)=dMean;
      }
    }
    //update gamma
    gamma+=beta*(D*z-d);
    //termination
    if(zLast.size()==z.size() && (zLast-z).cwiseAbs().maxCoeff()<eps() && (dLast-d).cwiseAbs().maxCoeff()<eps())
      break;
    if(zLast.size()==z.size())
      std::cout << "ADMMIter zUpdate=" << (zLast-z).cwiseAbs().maxCoeff() << " dUpdate=" << (dLast-d).cwiseAbs().maxCoeff() << std::endl;
    //record
    zLast=z;
    dLast=d;
  }
  return z;
}
//ExactSparseSmoothSolver
ExactSparseSmoothSolver::ExactSparseSmoothSolver(const SparseSmoothProblem& prob):_prob(prob),_lambda(1e-1) {}
const ExactSparseSmoothSolver::T& ExactSparseSmoothSolver::lambda() const {
  return _lambda;
}
ExactSparseSmoothSolver::T& ExactSparseSmoothSolver::lambda() {
  return _lambda;
}
ExactSparseSmoothSolver::T ExactSparseSmoothSolver::getBigM() const {
  Vec z=_prob.vss().row(2);
  T eLen=std::numeric_limits<T>::max();
  for(int i=0; i<_prob.iss().cols(); i++)
    for(int d=0; d<3; d++) {
      Vec2T a=_prob.xy(_prob.iss()(d,i));
      Vec2T b=_prob.xy(_prob.iss()((d+1)%3,i));
      eLen=std::min<T>(eLen,(a-b).norm());
    }
  return (z.maxCoeff()-z.minCoeff())/eLen;
}
ExactSparseSmoothSolver::Vec ExactSparseSmoothSolver::solve() const {
  GRBEnv env=GRBEnv(true);
  env.set("LogFile","mip1.log");
  env.start();
  // Create an empty model
  GRBModel model=GRBModel(env);
  GRBVar* dss=model.addVars(_prob.vss().cols(),GRB_CONTINUOUS);
  for(int i=0; i<_prob.vss().cols(); i++)
    dss[i].set(GRB_DoubleAttr_LB,-GRB_INFINITY);
  GRBVar* ess=model.addVars(_prob.ess().cols(),GRB_BINARY);
  model.update();
  //objective
  GRBQuadExpr expr=0;
  for(int i=0; i<_prob.ess().cols(); i++)
    expr+=ess[i]*lambda();
  SMatT H=_prob.getMass();
  Vec z=_prob.vss().row(2);
  for(int k=0; k<H.outerSize(); ++k)
    for(SMatT::InnerIterator it(H,k); it; ++it)
      expr+=(dss[it.row()]-z[it.row()])*(dss[it.col()]-z[it.col()])*it.value();
  model.setObjective(expr);
  model.update();
  //constraint
  T bigM=getBigM();
#define D1Var(R) dss[_prob.iss()(R,_prob.ess()(0,i))]
#define D2Var(R) dss[_prob.iss()(R,_prob.ess()(1,i))]
  for(int i=0; i<_prob.ess().cols(); i++) {
    Mat2X3T D1=_prob.getD(_prob.ess()(0,i));
    Mat2X3T D2=_prob.getD(_prob.ess()(1,i));
    for(int d=0; d<2; d++) {
      GRBLinExpr Ddiff=(D1(d,0)*D1Var(0)+D1(d,1)*D1Var(1)+D1(d,2)*D1Var(2))-(D2(d,0)*D2Var(0)+D2(d,1)*D2Var(1)+D2(d,2)*D2Var(2));
      model.addConstr( Ddiff<=ess[i]*bigM);
      model.addConstr(-Ddiff<=ess[i]*bigM);
    }
  }
  model.update();
#undef D1Var
#undef D2Var
  //solve
  model.optimize();
  Vec d=Vec::Zero(_prob.vss().cols());
  for(int i=0; i<d.size(); i++)
    d[i]=dss[i].get(GRB_DoubleAttr_X);
  return d;
}
//PatchLimitedExactSmoothSolver
std::pair<GRBLinExpr,GRBLinExpr> getD(GRBModel& model,GRBVar* Dss,int nD,PatchLimitedExactSmoothSolver::T bigM) {
  GRBVar* selector=model.addVars(nD/2);
  std::vector<double> weight(nD/2,1);
  model.addSOS(selector,weight.data(),nD/2,GRB_SOS_TYPE1);
  //new variable
  GRBVar* DssI=model.addVars(nD);
  for(int i=0; i<nD; i++)
    DssI[i].set(GRB_DoubleAttr_LB,-GRB_INFINITY);
  //assemble result
  std::pair<GRBLinExpr,GRBLinExpr> ret(0,0);
  for(int i=0; i<nD/2; i++) {
    ret.first+=DssI[i*2+0];
    ret.second+=DssI[i*2+1];
    //add constraint
    for(int d=0; d<2; d++) {
      model.addConstr(DssI[i*2+d]-Dss[i*2+d]<=(1-selector[i])*bigM);
      model.addConstr(Dss[i*2+d]-DssI[i*2+d]<=(1-selector[i])*bigM);
      model.addConstr( DssI[i*2+d]<=selector[i]*bigM);
      model.addConstr(-DssI[i*2+d]<=selector[i]*bigM);
    }
  }
  return ret;
}
PatchLimitedExactSmoothSolver::PatchLimitedExactSmoothSolver(const SparseSmoothProblem& prob):ExactSparseSmoothSolver(prob),_nPatch(5) {}
const int& PatchLimitedExactSmoothSolver::nPatch() const {
  return _nPatch;
}
int& PatchLimitedExactSmoothSolver::nPatch() {
  return _nPatch;
}
PatchLimitedExactSmoothSolver::Vec PatchLimitedExactSmoothSolver::solve() const {
  GRBEnv env=GRBEnv(true);
  env.set("LogFile","mip1.log");
  env.start();
  // Create an empty model
  T bigM=getBigM();
  GRBModel model=GRBModel(env);
  GRBVar* dss=model.addVars(_prob.vss().cols(),GRB_CONTINUOUS);
  for(int i=0; i<_prob.vss().cols(); i++)
    dss[i].set(GRB_DoubleAttr_LB,-GRB_INFINITY);
  GRBVar* Dss=model.addVars(nPatch()*2,GRB_CONTINUOUS);
  for(int i=0; i<nPatch()*2; i++)
    Dss[i].set(GRB_DoubleAttr_LB,-GRB_INFINITY);
#define DVar(R) dss[_prob.iss()(R,i)]
  for(int i=0; i<_prob.iss().cols(); i++) {
    std::pair<GRBLinExpr,GRBLinExpr> DI=getD(model,Dss,nPatch()*2,bigM);
    Mat2X3T D=_prob.getD(i);
    model.addConstr(D(0,0)*DVar(0)+D(0,1)*DVar(1)+D(0,2)*DVar(2)==DI.first);
    model.addConstr(D(1,0)*DVar(0)+D(1,1)*DVar(1)+D(1,2)*DVar(2)==DI.second);
  }
#undef DVar
  model.update();
  //objective
  GRBQuadExpr expr=0;
  SMatT H=_prob.getMass();
  Vec z=_prob.vss().row(2);
  for(int k=0; k<H.outerSize(); ++k)
    for(SMatT::InnerIterator it(H,k); it; ++it)
      expr+=(dss[it.row()]-z[it.row()])*(dss[it.col()]-z[it.col()])*it.value();
  model.setObjective(expr);
  model.update();
  //solve
  model.optimize();
  Vec d=Vec::Zero(_prob.vss().cols());
  for(int i=0; i<d.size(); i++)
    d[i]=dss[i].get(GRB_DoubleAttr_X);
  return d;
}
}
