from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

a,b,c,a0,b0,c0,alpha,beta,A=symbols('a b c a0 b0 c0 alpha beta A')
I=a*alpha+b*beta+c*(1-alpha-beta)
I0=a0*alpha+b0*beta+c0*(1-alpha-beta)
I=(I-I0)**2*A*2
#alpha
I=integrate(I,alpha)
I=I.subs(alpha,1-beta)-I.subs(alpha,0)
#beta
I=integrate(I,beta)
I=I.subs(beta,1)-I.subs(beta,0)
#energy
I=simplify(I)
print("Energy:",I)
print("Energy Eval:",simplify(I.subs(a0,a+1).subs(b0,b+1).subs(c0,c+1)))


#gradient
g=[diff(I,a),diff(I,b),diff(I,c)]
#hessian
for r in range(3):
    print(diff(g[r],a),diff(g[r],b),diff(g[r],c))